﻿using SampleMVVM.Models;
using SampleMVVM.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SampleMVVM.ViewModels
{
class LoginViewModel
{
    static public LoginViewModel vm = new LoginViewModel();
    public LoginModel model { get; set; }

    public LoginViewModel()
    {
        model = new LoginModel();

        LoginButtonCommand = new Command(async () => await LoginButtonClicked());
        SignUpCommand = new Command(SignUpClicked);
    }

    public ICommand LoginButtonCommand { get; set; }
    public ICommand SignUpCommand { get; set; }
       
    async Task LoginButtonClicked()
    {
        var r =  await LoginSrv.ValidateLogin();

        if (r)
        {
            Application.Current.MainPage.Navigation.PushAsync(new Views.UserPage());
        }
        else
        {
            Application.Current.MainPage.DisplayAlert("Login", "Login failed", "OK");
        }
    }

    void SignUpClicked()
    {
            Device.OpenUri(new Uri("http://www.Bing.com/"));
    }
}
}
