﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleMVVM.Models
{
    class LoginModel
    {
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
    }
}
