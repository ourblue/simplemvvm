﻿using SampleMVVM.Models;
using SampleMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SampleMVVM.Services
{
    class LoginSrv
    {
public static Task<bool> ValidateLogin()
{
    if (!string.IsNullOrEmpty(LoginViewModel.vm.model.UserEmail) &&
        !string.IsNullOrEmpty(LoginViewModel.vm.model.UserPassword))
    {
        if (LoginViewModel.vm.model.UserEmail.Contains("@")
            &&
            LoginViewModel.vm.model.UserPassword.Length > 1)
        {
            //   call backend services
            return Task.FromResult(true);
        }
    }
    return Task.FromResult(false);
}
    }
}
